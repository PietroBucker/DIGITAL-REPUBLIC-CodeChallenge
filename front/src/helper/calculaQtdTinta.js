function verificaQuantasLatas(conversaoLitros, latasDeTinta) {
  const tintaPequena = 0.5;
  const tintaMedia = 2.5;
  const tintaGrande = 3.6;
  const tintaGalao = 18;

  while (conversaoLitros > 0) {
    if (tintaGalao <= conversaoLitros) {
      conversaoLitros -= tintaGalao;
      latasDeTinta.galao += 1;
    } else if (tintaGrande <= conversaoLitros) {
      conversaoLitros -= tintaGrande;
      latasDeTinta.grande += 1;
    } else if (tintaMedia <= conversaoLitros) {
      conversaoLitros -= tintaMedia;
      latasDeTinta.media += 1;
    } else if (tintaPequena <= conversaoLitros) {
      conversaoLitros -= tintaPequena;
      latasDeTinta.pequena += 1;
    }
    if (conversaoLitros < 1) {
      conversaoLitros = Math.ceil(conversaoLitros * 2) / 2;
    }
  }

  return latasDeTinta;
// COMO NAO FOI ESPECIFICADO SOBRE SOBRA DA TINTA OPTEI POR FAZER O CALCULO DE FORMA QUE A SOBRA SEJA SEMPRE A MENOR POSSIVEL
}

export default function calculaQtdTinta(area) {
  const latasDeTinta = {
    galao: 0,
    grande: 0,
    media: 0,
    pequena: 0,
  };
  const rendimento = 5; // rende 5m² por litro
  const conversaoLitros = area / rendimento;

  return verificaQuantasLatas(conversaoLitros, latasDeTinta);
}
