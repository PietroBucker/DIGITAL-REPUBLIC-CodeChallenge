function areaPortas(n = 0) {
  const area = 1.52;
  if (n > 0) {
    return n * area;
  }
  return 0;
}

function areaJanelas(n = 0) {
  const area = 2.4;
  if (n > 0) {
    return n * area;
  }
  return 0;
}

export function areaParede({ width, height, doors, windows }) {
  const limiteAreaPortaJanela = 0.5;
  const area = width * height;
  const areaPortaJanela = areaPortas(doors) + areaJanelas(windows);
  if (areaPortaJanela / area > limiteAreaPortaJanela) {
    return 'Área de portas e(ou) janelas ultrapassou 50% da área da parede';
  }
  return area - areaPortaJanela;
}

export function areaTotalParedes(listaParedes) {
  return listaParedes.reduce((acc, cur) => acc + cur, 0);
}
