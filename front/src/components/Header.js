import React from 'react';
import '../styles/components/Header.css';
import logo from '../assets/11345393.png';

export default function Header() {
  return (
    <header>
      <img src={ logo } alt="lata de tinta" />
      <h1>
        Paint Calculator
      </h1>
    </header>
  );
}
