import React from 'react';
import '../styles/components/Input.css';
import propTypes from 'prop-types';

export default function Input({ labelName, ...rest }) {
  return (
    <div className="input_content">

      <label htmlFor={ rest.id }>{labelName}</label>
      <input
        className="input_personalized"
        { ...rest }
      />
    </div>
  );
}
Input.propTypes = {
  labelName: propTypes.string.isRequired,
};
