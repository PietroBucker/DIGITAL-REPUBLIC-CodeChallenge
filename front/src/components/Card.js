import React from 'react';
import propTypes from 'prop-types';
import '../styles/components/Card.css';

export default function Card({ props:
    { listaParedes, removeParede, areaTotal, qtdTinta } }) {
  return (
    <div className="card_content">
      <div className="card">
        {listaParedes.map((parede, ind) => {
          return (
            <div key={ ind } className="card_list_unit">
              <p>
                {parede}
                {' '}
                Metros²
              </p>
              <button
                className="button_remove_parede"
                onClick={ () => removeParede(ind) }
              >
                X
              </button>
            </div>
          );
        })}
      </div>

      <h2>Area Total</h2>
      <p>{`${areaTotal} Mertros²`}</p>
      <h2>Quantidade de Tinta</h2>
      <div className="card_resultado">
        <p>{`Galão 18L: ${qtdTinta.galao} Unidades`}</p>
        <p>{`Lata 3,6: ${qtdTinta.grande} Unidades`}</p>
        <p>{`Lata 2,5: ${qtdTinta.media} Unidades`}</p>
        <p>{`Lata 0,5: ${qtdTinta.pequena} Unidades`}</p>
      </div>
    </div>
  );
}

Card.propTypes = {
  props: propTypes.shape({
    listaParedes: propTypes.arrayOf(propTypes.number).isRequired,
    removeParede: propTypes.func.isRequired,
    areaTotal: propTypes.number.isRequired,
    qtdTinta: propTypes.shape({
      galao: propTypes.number.isRequired,
      grande: propTypes.number.isRequired,
      media: propTypes.number.isRequired,
      pequena: propTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
};
