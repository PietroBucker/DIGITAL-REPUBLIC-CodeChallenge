import React, { useContext } from 'react';
import { FormContext } from '../context/contetxtForm';
import Input from './Input';
import Card from './Card';

import '../styles/components/Form.css';
// import areaParedes from '../helper/calculaArea';
/*
[x] nenhuma parede poder ter menos de 1 metro nem mais de 50 metros mas podem possuir medidas diferentes
[x] total de area das portas e janela nao podem ser maior que 50% da area da parede
[x] cada janela possui 2,00 x 1,20
[x] cada porta possui 0,80 x 1,90
[] cada litro de tintar pinta 5m²
[] sempre considerar da maior lata para a menor
tamnho das latas
  0,5L
  2,5L
  3,6L
  18L
*/

export default function Form() {
  const {
    state,
    areaTotal,
    listaParedes,
    qtdTinta,
    errMessage,
    handleChange,
    addParede,
    removeParede,
  } = useContext(FormContext);

  return (
    <form className="form_content" onSubmit={ (e) => e.preventDefault() }>
      <div className="form_inputs">

        <Input
          id="width"
          labelName="Largura da Parede"
          type="number"
          name="width"
          value={ state.width }
          min="1"
          max="50"
          onChange={ handleChange }
        />
        <Input
          id="height"
          labelName="Altura da Parede"
          type="number"
          name="height"
          min="1"
          max="50"
          value={ state.height }
          onChange={ handleChange }
        />

        <Input
          id="doors"
          labelName="Numero de Portas"
          type="number"
          name="doors"
          value={ state.doors }
          onChange={ handleChange }
        />

        <Input
          id="windows"
          labelName="Numero de Janelas"
          type="number"
          name="windows"
          value={ state.windows }
          onChange={ handleChange }
        />

        <button
          className="button_add_parede"
          onClick={ () => {
            if (state.width !== 0 && state.height !== 0) {
              addParede();
            }
            return console
              .log('necessario preencher a largura e altura da parede');
          } }
        >
          Adicionar Parede
        </button>
        {errMessage && <p>{errMessage}</p>}
      </div>

      <Card props={ { listaParedes, removeParede, areaTotal, qtdTinta } } />

    </form>
  );
}
