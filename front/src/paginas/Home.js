import React from 'react';
import Header from '../components/Header';
import '../styles/Home.css';
import Form from '../components/Form';
import Footer from '../components/Footer';
import FormProvider from '../context/contetxtForm';

export default function Home() {
  return (
    <div className="home_content">
      <Header />
      <FormProvider>
        <Form />
      </FormProvider>
      <Footer />
    </div>
  );
}
