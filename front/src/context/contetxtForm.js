/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, createContext, useMemo, useState } from 'react';
import propTypes from 'prop-types';
import { areaParede, areaTotalParedes } from '../helper/calculaArea';
import calculaQtdTinta from '../helper/calculaQtdTinta';

export const FormContext = createContext();

const araeInitialState = {
  width: 0,
  height: 0,
  doors: 0,
  windows: 0,
};

const latasDeTintaInitialState = {
  galao: 0,
  grande: 0,
  media: 0,
  pequena: 0,
};

export default function FormProvider({ children }) {
  const [state, setState] = useState(araeInitialState);
  const [areaTotal, setAreaTotal] = useState(0);
  const [listaParedes, setListaParedes] = useState([]);
  const [qtdTinta, setQtdTinta] = useState(latasDeTintaInitialState);
  const [errMessage, setErrMessage] = useState('');

  const handleChange = ({ target }) => {
    setState((prevState) => {
      return {
        ...prevState,
        [target.name]: Number(target.value),
      };
    });
  };

  const addParede = () => {
    const area = areaParede(state);
    if (typeof area === 'string') {
      setErrMessage(area);
      return;
    }
    setListaParedes((prevState) => {
      return [...prevState, area];
    });
    setErrMessage('');
  };

  const removeParede = (index) => {
    const listaAtulizada = listaParedes.filter((_, ind) => ind !== index);
    setListaParedes(listaAtulizada);
  };

  const calculaAreaTotal = () => {
    const area = areaTotalParedes(listaParedes);
    const litrosTinta = calculaQtdTinta(area);
    setAreaTotal(area);
    setQtdTinta(litrosTinta);
  };

  useEffect(() => {
    calculaAreaTotal();
    console.log('render');
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [listaParedes]);

  const value = useMemo(() => ({
    state,
    setState,
    areaTotal,
    listaParedes,
    qtdTinta,
    errMessage,
    handleChange,
    addParede,
    removeParede,
  }), [
    state, setState, areaTotal,
    listaParedes, qtdTinta, errMessage]);
  return (
    <FormContext.Provider
      value={ value }
    >
      {children}
    </FormContext.Provider>
  );
}

FormProvider.propTypes = {
  children: propTypes.node.isRequired,
};
