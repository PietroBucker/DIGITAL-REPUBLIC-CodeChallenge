# Projeto Desafio

## O que foi desenvolvido

Uma aplicação web ou mobile que ajuda o usuário a calcular a quantidade de tinta necessária para pintar uma sala. Essa aplicação considera que a sala é composta de 4 paredes e permite que o usuário escolha as medidas de cada parede e quantas janelas e portas cada parede possui.

Com base na quantidade necessária, o sistema aponta os tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Por exemplo, se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L.

## Regras de negócio

- Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.
- O total de área das portas e janelas deve ser no máximo 50% da área de parede.
- A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.
- Cada janela possui as medidas: 2,00 x 1,20 metros.
- Cada porta possui as medidas: 0,80 x 1,90 metros.
- Cada litro de tinta é capaz de pintar 5 metros quadrados.
- Não considerar teto nem piso.

### Variações de tamanho das latas de tinta:

- 0,5 L
- 2,5 L
- 3,6 L
- 18 L

## Como usar na sua máquina

1. Clone o repositório
    ```bash
    git clone [URL_DO_REPOSITORIO]
    ```
2. Instale as dependências com npm
    ```bash
    npm install
    ```
3. Rode a aplicação React
    ```bash
    npm start
    ```
### Foi usado:

- React
- Context